import React, { memo } from "react";
import { TouchableNativeFeedback, TouchableNativeFeedbackProps, View } from "react-native";

import Icon from "react-native-vector-icons/Feather";
import {
    Container,
    Item,
    Touchable,
    TouchableExit,
    ViewLeft,
    ViewRight,
    TapToEdit,
    Text,
    TextName
} from "./styles";

interface IListItem extends TouchableNativeFeedbackProps {
    name: string;
    age: number;
    cpf: string;
    bairro: string;
    removeItem: Function;
}

const ListItem = ({ name, age, cpf, bairro, removeItem, ...rest }: IListItem) => {
    return (
        <Container>
            <Touchable {...rest} background={TouchableNativeFeedback.Ripple(
                '#CCD5DE',
                false
            )}>
                <Item>
                    <ViewLeft>
                        <TextName>{name ? name : 'Não informado'}</TextName>
                        <Text>{age ? `${age} anos` : 'Idade não informada'}</Text>
                        <Text>{cpf ? 'CPF: ' + cpf : 'CPF não informado'}</Text>
                        <Text>{bairro ? bairro : 'Local não informado'}</Text>
                    </ViewLeft>
                    <View style={{ alignItems: 'flex-end' }}>
                        <TouchableExit onPress={() => removeItem()}>
                            <Icon name="x" color="black" size={25} />
                        </TouchableExit>
                        <ViewRight>
                            <TapToEdit>Toque para editar</TapToEdit>
                        </ViewRight>
                    </View>
                </Item>
            </Touchable>
        </Container>
    )
}

export default memo(ListItem);