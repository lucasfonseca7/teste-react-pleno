import styled from "styled-components/native";

export const Touchable = styled.TouchableNativeFeedback`
`;

export const TouchableExit = styled.TouchableOpacity`
`;

export const Container = styled.View`
    overflow: hidden;
`;

export const Item = styled.View`
    padding: 16px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    background: white;
    margin: 8px;
    border-radius: 8px;
    elevation: 3;
`;

export const ViewLeft = styled.View`
    flex: 1;
    padding-left: 4px;
`
export const ViewRight = styled.View`
    flex: 1;
    padding-top: 8px;
`;

export const TapToEdit = styled.Text`
    color: #3A84FF;
    padding-right: 12px;
    font-weight: bold;
    font-size: 16px;
`;

export const Text = styled.Text`
    color: #454545;
    font-weight: normal;
    font-size: 16px;
`;

export const TextName = styled.Text`
    color: #454545;
    font-weight: bold;
    font-size: 18px;
`;