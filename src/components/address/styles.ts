import styled from "styled-components/native";

export const Text = styled.Text`
    font-size: 16px;
    padding-left: 4px;
`;

export const Item = styled.View`
    justify-content: flex-start;
    align-items: baseline;
    padding-right: 16px;
    padding-left: 16px;
    padding-bottom: 16px;
`;

export const Title = styled.Text`
    font-weight: bold;
    font-size: 18px;
    color: #303030;
    padding-left: 4px;
`;