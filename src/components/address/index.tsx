import React, { memo, useMemo } from "react";

import { Item, Text, Title } from "./styles";

interface ILocation {
    cep?: string;
    bairro?: string;
    localidade?: string;
    logradouro?: string;
}

const Address = ({ cep, bairro, localidade, logradouro }: ILocation) => {

    const cepMemo = useMemo(() => cep, [cep]);
    const bairroMemo = useMemo(() => bairro, [bairro]);
    const localidadeMemo = useMemo(() => localidade, [localidade]);
    const logradouroMemo = useMemo(() => logradouro, [logradouro]);

    return (
        <Item>
            <Title>Endereço</Title>
            <Text>
                CEP: {cepMemo && cepMemo !== ' ' ? cepMemo : 'Não encontrado'}
            </Text>
            <Text>
                Bairro: {bairroMemo && bairroMemo !== ' ' ? bairroMemo : 'Não encontrado'}
            </Text>
            <Text>
                Cidade: {localidadeMemo && localidadeMemo !== ' ' ? localidadeMemo : 'Não encontrado'}
            </Text>
            <Text>
                Rua: {logradouroMemo && logradouroMemo !== ' ' ? logradouroMemo : 'Não encontrado'}
            </Text>
        </Item>
    );
}

export default memo(Address);