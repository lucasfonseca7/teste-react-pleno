import { StyleSheet } from "react-native";
import styled, { css } from "styled-components/native";

interface IButton {
    color?: string;
}

export const Touchable = styled.TouchableNativeFeedback`
`;

export const Item = styled.View`
    ${(props: IButton) => css`
        background-color: ${props.color ? props.color : '#1EB300'};
    `}
    padding: 16px;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-radius: 16px;
    elevation: 3;
    overflow:hidden;
`;

export const Text = styled.Text`
    color: #FFF;
    padding-right: 16px;
    text-transform: uppercase;
    font-weight: bold;
`;
