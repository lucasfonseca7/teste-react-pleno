import React, { memo } from "react";
import {
    TouchableNativeFeedback,
    TouchableNativeFeedbackProps,
    ActivityIndicator,
    View
} from "react-native";
import { Item, Touchable, Text } from "./styles";

interface IButton extends TouchableNativeFeedbackProps {
    text: string;
    color?: string;
    loading?: boolean;
}

const Button = ({ text, color, loading, ...rest }: IButton) => {
    return (
        <>
            <Touchable {...rest} background={TouchableNativeFeedback.Ripple(
                '#CCD5DE',
                false
            )}><Item>
                    {
                        loading ? (
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size="small" color="white" />
                            </View>) : (
                                <Text>{text}</Text>
                            )
                    }
                </Item></Touchable>
        </>
    )
}

export default memo(Button);