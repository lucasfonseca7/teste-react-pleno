import React, { memo, ReactNode } from "react";
import { View, TouchableOpacity } from "react-native";
import { Container, Item, EditText, FieldContainer, TitleContent } from "./styles";

interface IFieldUser {
    title: string;
    children: ReactNode;
    tapEdit: Function;
}

const FieldUser = ({ title, tapEdit, children }: IFieldUser) => {
    return (
        <Container>
            <TitleContent>{title}</TitleContent>
            <Item>
                <FieldContainer>
                    {children}
                </FieldContainer>
                <View>
                    <TouchableOpacity onPress={() => tapEdit()}><EditText>Editar</EditText></TouchableOpacity>
                </View>
            </Item>
        </Container>
    );
}

export default memo(FieldUser);