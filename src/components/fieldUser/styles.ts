import styled, { css } from "styled-components/native";

interface IFieldUser {
    noFlexDirectionRow?: boolean;
    noCenter?: boolean;
}

export const Container = styled.View``;

export const Item = styled.View`
    justify-content: space-between;
    flex-direction: row;
    align-items: center;
    padding-top: 4px;
    padding-right: 8px;
    padding-left: 16px;
    padding-bottom: 8px;
    border-radius: 8px;
`;

export const EditText = styled.Text`
    color: #3A84FF;
    font-size: 16px;
`;

export const FieldContainer = styled.View`
    flex: 1;
    height: 60px;
    padding-right: 8px;
`;

export const TitleContent = styled.Text`
    font-weight: bold;
    font-size: 18px;
    color: #303030;
    padding-left: 18px;
`;