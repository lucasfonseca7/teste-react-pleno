import styled, { css } from 'styled-components/native';

interface ITextField {
  border?: boolean;
  isFilled?: boolean;
}

const TextInput = styled.TextInput`
  flex: 1;
  height: 50px;
  /* border-left-color: transparent;
  border-right-color: transparent;
  border-top-color: transparent; */
  border-radius: 8px;
  padding-left: 16px;
  ${(props: ITextField) => css`
    border-width: ${props.border || props.isFilled ? 2 : 1}px;
    border-color: ${props.border || props.isFilled
      ? '#59B1F0'
      : '#e9e9e9'};
  `}
  width: 100%;
  font-size: 18px;
`;

export { TextInput };
