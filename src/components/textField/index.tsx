import React, {
    forwardRef,
    useImperativeHandle,
    useRef,
    useCallback,
    useState,
    useEffect,
} from 'react';
import { TextInputProps, View, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Feather";

import { TextInput } from './styles';

interface ITextField extends TextInputProps {
    borderFocus?: boolean;
    clear?: Function;
    activeClear?: boolean;
    value?: string;
}

const TextField = ({ borderFocus, value, clear, activeClear, ...rest }: ITextField, ref: any) => {
    const inputElementRef = useRef<any>();

    const [isFocused, setIsFocused] = useState<boolean>();
    const [isFilled, setIsFilled] = useState<boolean>();

    useEffect(() => {
        if (value && value !== '') {
            setIsFilled(true);
        } else {
            setIsFilled(false);
        }
    }, [value]);

    useImperativeHandle(ref, () => ({
        focus() {
            inputElementRef.current.focus();
        },
        clear() {
            resetInputField();
        }
    }));

    const handleInputFocus = useCallback(() => {
        setIsFocused(true);
    }, []);

    const handleInputBlur = useCallback(() => {
        setIsFocused(false);
    }, []);

    const resetInputField = useCallback(() => {
        inputElementRef.current.clear();
    }, []);

    return (
        <View style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <TextInput
                ref={inputElementRef}
                placeholderTextColor={isFocused ? '#59B1F0' : '#ADB0B3'}
                border={isFocused}
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                isFilled={isFilled}
                {...rest}
            />
            {activeClear ?
                <TouchableOpacity disabled={!isFilled} onPress={resetInputField}>
                    <Icon name="x-circle" size={20} color={!isFilled ? "#cdcdcd" : "#3A84FF"} />
                </TouchableOpacity>
                : null}
        </View>
    );
};

export default forwardRef(TextField);
