import React, { memo, useState, useRef, useCallback, useEffect } from "react";
import { ToastAndroid } from "react-native";
import { TextField, Button, Address, Modal } from "components";
import getRealm from "database";
import {
    Scaffold,
    Title,
    Content,
    Footer,
    Container,
    Line,
    ContentModal,
    TextModal,
    ButtonModal,
    TextButtonModal
} from "./styles";
import { GET_CEP } from "api/address";
import NetState from '@react-native-community/netinfo';
interface ILocation {
    cep: string;
    bairro: string;
    localidade: string;
    logradouro: string;
}

interface IUser {
    name?: string;
    age?: number;
    cpf?: string;
    rg?: string;
}

const RegisterScreen = () => {
    const [address, setAddress] = useState<ILocation>();
    const [user, setUser] = useState<IUser>();
    const [loading, setLoading] = useState(false);
    const [isVisibleModal, setModalVisible] = useState(false);
    const [cep, setCep] = useState('');
    const refName = useRef<any>();
    const refIdade = useRef<any>();
    const refCpf = useRef<any>();
    const refRg = useRef<any>();
    const refCep = useRef<any>();

    const createIndex = useCallback(async () => {
        const realm = await getRealm();
        let table = await realm.objects('User').sorted('id', true);
        if (table.length < 1) {
            return 1;
        }
        let highestId = table[0].id;
        return highestId + 1;
    }, []);

    const resetField = useCallback(() => {
        refName.current.clear();
        refIdade.current.clear();
        refCpf.current.clear();
        refRg.current.clear();
        refCep.current.clear();
        setUser({
            name: '',
            age: 0,
            cpf: '',
            rg: ''
        });
        setAddress({
            cep: '',
            bairro: '',
            localidade: '',
            logradouro: ''
        });
        setCep('');
    }, [user]);

    const notifyMessage = useCallback(() => {
        ToastAndroid.show(
            'Internet não identificada. conecte-se para carregar o endereço.',
            ToastAndroid.CENTER,
        );
    }, []);

    const handleAddUser = useCallback(async () => {
        try {
            setLoading(true);
            const index = await createIndex();
            const realm = await getRealm();
            realm.write(() => {
                realm.create('User', {
                    id: index,
                    name: user ? user.name : '',
                    age: user ? user.age : 0,
                    cpf: user ? user.cpf : '',
                    rg: user ? user.rg : '',
                    address: {
                        id: index,
                        cep: address && address.cep !== '' ? address.cep : '',
                        bairro: address && address.bairro !== '' ? address.bairro : '',
                        localidade: address && address.localidade !== '' ? address.localidade : '',
                        logradouro: address && address.logradouro !== '' ? address.logradouro : ''
                    }
                });
            });
            resetField();
            setLoading(false);
            setModalVisible(true)
        } catch (err) {
            console.log(err);
        }
    }, [user, address]);

    useEffect(() => {
        if (cep.length === 8) {
            NetState.fetch().then((netState) => {
                if (netState.isConnected) {
                    GET_CEP({ cep: cep }).then(result => {
                        result && result.erro === true ?
                            setAddress({
                                bairro: '',
                                cep: '',
                                localidade: '',
                                logradouro: ''
                            })
                            :
                            setAddress(result)
                    });
                } else {
                    notifyMessage()
                }
            }
            );
        }
    }, [cep]);

    const closeModal = useCallback(() => {
        setModalVisible(false);
    }, [isVisibleModal]);

    return (
        <Scaffold>
            <Container>
                <Title>Adicione uma pessoa</Title>
                <Content>
                    <TextField
                        ref={refName}
                        autoCapitalize="none"
                        onChangeText={(text) => setUser({ ...user, name: text })}
                        returnKeyType="next"
                        placeholder="Digite o nome..."
                        value={user?.name}
                        keyboardType="default"
                    />
                    <Line></Line>
                    <TextField
                        ref={refIdade}
                        autoCapitalize="none"
                        onChangeText={(text) => setUser({ ...user, age: parseInt(text) })}
                        returnKeyType="next"
                        placeholder="Digite a idade..."
                        value={user?.age}
                        maxLength={3}
                        keyboardType="number-pad"
                    />
                    <Line></Line>
                    <TextField
                        ref={refCpf}
                        autoCapitalize="none"
                        onChangeText={(text) => setUser({ ...user, cpf: text })}
                        returnKeyType="next"
                        placeholder="Digite o CPF..."
                        value={user?.cpf}
                        maxLength={11}
                        keyboardType="number-pad"
                    />
                    <Line></Line>
                    <TextField
                        ref={refRg}
                        autoCapitalize="none"
                        onChangeText={(text) => setUser({ ...user, rg: text })}
                        returnKeyType="next"
                        placeholder="Digite o RG..."
                        value={user?.rg}
                        maxLength={9}
                        keyboardType="number-pad"
                    />
                    <Line></Line>
                    <TextField
                        ref={refCep}
                        autoCapitalize="none"
                        onChangeText={(text) => setCep(text)}
                        returnKeyType="next"
                        placeholder="Digite o CEP..."
                        value={cep}
                        maxLength={8}
                        keyboardType="numeric"
                        onSubmitEditing={handleAddUser}
                    />
                    <Line></Line>
                    <Address bairro={address?.bairro} cep={address?.cep} localidade={address?.localidade} logradouro={address?.logradouro} />
                    <Footer>
                        <Button loading={loading} onPress={handleAddUser} text="Salvar" />
                    </Footer>
                </Content>
                <Modal noPadding isVisible={isVisibleModal}>
                    <ContentModal>
                        <TextModal>Cadastrado com sucesso</TextModal>
                        <ButtonModal onPress={closeModal}><TextButtonModal>FECHAR</TextButtonModal></ButtonModal>
                    </ContentModal>
                </Modal>
            </Container>
        </Scaffold>
    )
}

export default memo(RegisterScreen);