import styled from "styled-components/native";

export const Scaffold = styled.ScrollView`
    flex: 1;
    width: 100%;
    background: white;
`;

export const Container = styled.ScrollView`
    flex: 1;
    width: 100%;
    background: #014EF5;
    padding-top: 16px;
`;

export const Title = styled.Text`
    font-weight: bold;
    font-size: 22px;
    text-align: center;
    margin-bottom: 16px;
    color: white;
`;

export const Content = styled.View`
    flex: 1;
    background: white;
    border-top-left-radius: 32px;
    border-top-right-radius: 32px;
    padding: 16px;
    padding-top: 24px;
`;

export const Line = styled.View`
    margin-top: 8px;
`;

export const Footer = styled.View`
    margin-top: 16px;
    background: white;
`;

export const ContentModal = styled.View`
    /* flex: 1; */
    padding: 16px;
`;

export const TextModal = styled.Text`
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 16px;
`;

export const ButtonModal = styled.TouchableOpacity`
    border-radius: 16px;
    background: #1EB300;
    align-items: center;
    padding: 8px;
`;

export const TextButtonModal = styled.Text`
    color: white;
    font-size: 16px;
    font-weight: bold;
`;