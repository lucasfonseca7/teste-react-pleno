import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import { useRoute } from "@react-navigation/native";
import NetState from '@react-native-community/netinfo';
import { GET_CEP } from "api/address";

import { FieldUser, Button, Address, TextField, Modal } from "components";

import {
    Scaffold,
    Title,
    Content,
    Container,
    ContentModal,
    TextModal,
    ButtonModal,
    TextButtonModal
} from "./styles";
import getRealm from "database";
import { ToastAndroid } from "react-native";

interface IAddress {
    cep: string;
    bairro: string;
    localidade: string;
    logradouro: string;
}

interface IUser {
    id: number;
    name: string;
    age: number;
    cpf: string;
    rg: string;
    address: IAddress;
}

interface ILocation {
    cep: string;
    bairro: string;
    localidade: string;
    logradouro: string;
}

const EditScreen = () => {
    const { params } = useRoute();
    const [user, setUser] = useState<IUser | object>({});
    const [isVisibleModal, setModalVisible] = useState(false);
    const [address, setAddress] = useState<ILocation>();
    const [name, setName] = useState('');
    const [age, setAge] = useState(0);
    const [cpf, setCpf] = useState('');
    const [rg, setRg] = useState('');
    const [cep, setCep] = useState('');
    const [editName, setEditName] = useState(false);
    const [editAge, setEditAge] = useState(false);
    const [editCpf, setEditCpf] = useState(false);
    const [editRg, setEditRg] = useState(false);
    const [editCep, setEditCep] = useState(false);
    const refName = useRef<any>();
    const refAge = useRef<any>();
    const refCpf = useRef<any>();
    const refRg = useRef<any>();
    const refCep = useRef<any>();

    useEffect(() => {
        setUser(params.user);
    }, [params.user]);

    const closeModal = useCallback(() => {
        setModalVisible(false);
    }, [isVisibleModal]);

    const handleUpdateUser = useCallback(async () => {
        try {
            const realm = await getRealm();
            const userMatched = await realm.objects('User').find(({ id }) => id === user.id);
            realm.write(() => {
                realm.create('User', {
                    id: userMatched.id,
                    name: name ? name : user.name,
                    age: age ? age : user.age,
                    cpf: cpf ? cpf : user.cpf,
                    rg: rg ? rg : user.rg,
                    address: {
                        id: userMatched.id,
                        cep: address?.cep,
                        bairro: address?.bairro,
                        localidade: address?.localidade,
                        logradouro: address?.logradouro
                    }
                }, 'modified');
            });
            setModalVisible(true);
        } catch (err) {
            console.log(err)
        }
    }, [user.id, name, age, cpf, rg, user.address]);

    const handleEditName = useCallback(() => {
        setEditName(true);
        refName.current.focus();
    }, []);

    const handleEditAge = useCallback(() => {
        setEditAge(true);
        refAge.current.focus();
    }, []);

    const handleEditCpf = useCallback(() => {
        setEditCpf(true);
        refCpf.current.focus();
    }, []);

    const handleEditRg = useCallback(() => {
        setEditRg(true);
        refRg.current.focus();
    }, []);

    const handleEditCep = useCallback(() => {
        setEditCep(true);
        refCep.current.focus();
    }, []);

    const notifyMessage = useCallback(() => {
        ToastAndroid.show(
            'Internet não identificada. conecte-se para carregar o endereço.',
            ToastAndroid.CENTER,
        );
    }, []);

    useEffect(() => {
        if (cep.length === 8) {
            NetState.fetch().then((netState) => {
                if (netState.isConnected) {
                    GET_CEP({ cep: cep }).then(result => {
                        result && result.erro === true ?
                            setAddress({
                                bairro: '',
                                cep: '',
                                localidade: '',
                                logradouro: ''
                            })
                            :
                            setAddress(result)
                    });
                } else {
                    notifyMessage()
                }
            }
            );
        }
    }, [cep]);

    return (
        <Scaffold>
            <Container>
                <Title>Editar pessoa</Title>
                <Content>
                    <FieldUser title="Nome" tapEdit={handleEditName}>
                        <TextField
                            ref={refName}
                            autoCapitalize="none"
                            onChangeText={(text) => setName(text)}
                            returnKeyType="next"
                            placeholder={user.name}
                            activeClear={false}
                            value={name}
                            keyboardType="default"
                            editable={editName}
                            onSubmitEditing={() => refAge.current.focus()}
                        />
                    </FieldUser>
                    <FieldUser title="Idade" tapEdit={handleEditAge}>
                        <TextField
                            ref={refAge}
                            onChangeText={(text) => setAge(parseInt(text))}
                            returnKeyType="next"
                            placeholder={user.age ? user.age.toString() : ''}
                            activeClear={false}
                            value={age ? age.toString() : ''}
                            maxLength={3}
                            keyboardType="numeric"
                            editable={editAge}
                            onSubmitEditing={() => refCpf.current.focus()}
                        />
                    </FieldUser>
                    <FieldUser title="CPF" tapEdit={handleEditCpf}>
                        <TextField
                            ref={refCpf}
                            onChangeText={(text) => setCpf(text)}
                            returnKeyType="next"
                            placeholder={user.cpf}
                            activeClear={false}
                            value={cpf}
                            maxLength={11}
                            keyboardType="numeric"
                            editable={editCpf}
                            onSubmitEditing={() => refRg.current.focus()}
                        />
                    </FieldUser>
                    <FieldUser title="RG" tapEdit={handleEditRg}>
                        <TextField
                            ref={refRg}
                            onChangeText={(text) => setRg(text)}
                            returnKeyType="next"
                            placeholder={user.rg}
                            activeClear={false}
                            value={rg}
                            maxLength={9}
                            keyboardType="numeric"
                            editable={editRg}
                            onSubmitEditing={() => refCep.current.focus()}
                        />
                    </FieldUser>
                    <FieldUser title="CEP" tapEdit={handleEditCep}>
                        <TextField
                            ref={refCep}
                            onChangeText={(text) => setCep(text)}
                            returnKeyType="next"
                            placeholder={user.address ? user.address.cep : ''}
                            activeClear={false}
                            value={cep}
                            maxLength={8}
                            keyboardType="numeric"
                            editable={editCep}
                            onSubmitEditing={handleUpdateUser}
                        />
                    </FieldUser>
                    <Address bairro={address ? address.bairro : user.address ? user.address.bairro : ''}
                        cep={address ? address.cep : user.address ? user.address.cep : ''}
                        logradouro={address ? address.logradouro : user.address ? user.address.logradouro : ''}
                        localidade={address ? address.localidade : user.address ? user.address.localidade : ''} />
                    <Button text="Salvar" onPress={handleUpdateUser} />
                </Content>
                <Modal noPadding isVisible={isVisibleModal}>
                    <ContentModal>
                        <TextModal>Cadastro editado com sucesso</TextModal>
                        <ButtonModal onPress={closeModal}><TextButtonModal>FECHAR</TextButtonModal></ButtonModal>
                    </ContentModal>
                </Modal>
            </Container>
        </Scaffold>
    )
}

export default memo(EditScreen);