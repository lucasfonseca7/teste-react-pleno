import styled, { css } from "styled-components/native";

export const Scaffold = styled.ScrollView`
    flex: 1;
    background: #014EF5;
`;

export const Container = styled.View`
    padding-top: 16px;
`

export const Title = styled.Text`
    color: #FFF;
    font-size: 20px;
    font-weight: bold;
    text-align: center;
`;

export const Content = styled.View`
    flex: 1;
    background: white;
    border-top-left-radius: 32px;
    border-top-right-radius: 32px;
    padding: 16px;
    margin-top: 16px;
`;

export const ContentModal = styled.View`
    /* flex: 1; */
    padding: 16px;
`;

export const TextModal = styled.Text`
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 16px;
`;

export const ButtonModal = styled.TouchableOpacity`
    border-radius: 16px;
    background: #1EB300;
    align-items: center;
    padding: 8px;
`;

export const TextButtonModal = styled.Text`
    color: white;
    font-size: 16px;
    font-weight: bold;
`;