import styled, { css } from "styled-components/native";
import { StyleSheet } from "react-native";

interface IButton {
    color: string;
    side: string;
}

export const Container = styled.View`
    flex: 1;
`;

export const ViewPullUpdate = styled.View`
    align-items: center;
    padding-top: 8px;
`;

export const TextPullUpdate = styled.Text`
    font-weight: bold;
    font-size: 20px;
    text-align: center;
    color: #1774FF;
`;

export const Line = styled.View`
    height: ${StyleSheet.hairlineWidth}px;
    width: 100%;
    background: #cdcdcd;
`;

export const ViewEmpty = styled.View`
    align-items: center;
    justify-content: center;
    padding: 16px;
    margin-top: 64px;
`;

export const TitleEmpty = styled.Text`
    font-weight: bold;
    font-size: 28px;
    text-align: center;
    color: #323232;
`;

export const ContentModal = styled.View`
    flex-direction: row;
`;

export const ContentButtonModal = styled.View`
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
`;

export const ButtonModal = styled.TouchableOpacity`
    flex: 1;
    align-items: center;
    padding: 16px;
    ${(props: IButton) => css`
        background: ${props.color ? props.color : 'green'}
        border-bottom-left-radius: ${props.side === 'left' ? 24 : 0}px;
        border-bottom-right-radius: ${props.side === 'right' ? 24 : 0}px;
        border-left-width: ${props.side === 'right' ? StyleSheet.hairlineWidth : 0}px;
        border-left-color: #a9a9a9;
    `}
`;

export const TextButtonModal = styled.Text`
    font-size: 16px;
    color: white;
    font-weight: bold;
`;

export const TitleModal = styled.Text`
    padding: 16px;
    font-weight: bold;
    font-size: 20px;
    text-align: center;
    color: #323232;
`;