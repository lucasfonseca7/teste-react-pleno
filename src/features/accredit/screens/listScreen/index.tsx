import React, { memo, useCallback, useEffect, useState } from "react";
import getRealm from "database";
import { FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { ListItem } from "components";
import Icon from "react-native-vector-icons/Feather";
import {
    Container,
    ViewEmpty,
    TitleEmpty,
    ContentModal,
    ContentButtonModal,
    ButtonModal,
    TextButtonModal,
    TitleModal,
    ViewPullUpdate,
    TextPullUpdate
} from "./styles";
import { Modal } from "components";

const ListScreen = () => {
    const [user, setUsers] = useState([]);
    const [isModalVisible, setModalVisible] = useState(false);
    const [userId, setUserId] = useState(0);
    const { navigate } = useNavigation();

    useEffect(() => {
        const getUsers = async () => {
            try {
                const realm = await getRealm();
                const users = await realm.objects('User');
                setUsers(users);
            } catch (err) {
                console.log(err)
            }
        }
        getUsers();
    }, []);

    const getUser = useCallback(async () => {
        try {
            const realm = await getRealm();
            const users = await realm.objects('User');
            setUsers(users);
        } catch (err) {
            console.log(err)
        }
    }, [user]);

    const handleRemoveUser = useCallback(async (userId: number) => {
        try {
            const realm = await getRealm();
            const userMatched = await realm.objects('User').find(({ id }) => id === userId);
            realm.write(() => {
                realm.delete(userMatched.address);
                realm.delete(userMatched);
            });
            setModalVisible(false);
            getUser();
        } catch (err) {
            console.log(err)
        }
    }, [user.id]);

    const navigateTo = useCallback((item: object) => {
        navigate('EditScreen', { user: item });
    }, []);

    const closeModal = useCallback(() => {
        setModalVisible(false);
    }, [isModalVisible]);

    const openModal = useCallback((id: number) => {
        setUserId(id);
        setModalVisible(true);
    }, [isModalVisible]);

    return (
        <>
            <Container>
                <FlatList
                    data={user}
                    ListEmptyComponent={() => (<ViewEmpty><Icon name="user-x" color="#014EF5" size={48} /><TitleEmpty>Nenhum usuário cadastrado</TitleEmpty></ViewEmpty>)}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => {
                        return (
                            <>
                                {index === 0 ?
                                    (<ViewPullUpdate style={{ flex: 1 }}>
                                        <TextPullUpdate>Puxe para atualizar</TextPullUpdate>
                                    </ViewPullUpdate>)
                                    : null
                                }
                                <ListItem
                                    onPress={() => navigateTo(item)}
                                    removeItem={() => openModal(item.id)}
                                    name={item.name}
                                    bairro={item.address ? item.address.bairro : ''}
                                    age={item.age}
                                    cpf={item.cpf} />
                            </>
                        );
                    }}
                    onRefresh={() => getUser()}
                    refreshing={false}
                />
            </Container>
            <Modal noPadding isVisible={isModalVisible}>
                <TitleModal>Tem certeza que deseja remover o item?</TitleModal>
                <ContentModal>
                    <ContentButtonModal>
                        <ButtonModal activeOpacity={0.6} side="left" color="#237CE8" onPress={closeModal}><TextButtonModal>NÃO</TextButtonModal></ButtonModal>
                        <ButtonModal activeOpacity={0.6} side="right" color="#237CE8" onPress={() => handleRemoveUser(userId)}><TextButtonModal>SIM</TextButtonModal></ButtonModal>
                    </ContentButtonModal>
                </ContentModal>
            </Modal>
        </>
    )
}

export default memo(ListScreen);