import React, { memo, useState, useCallback } from "react";
import { ActivityIndicator, Text } from "react-native";
import getRealm from "database";

import Icons from "react-native-vector-icons/Feather";
import { Container, Title, Card, Subtitle, Content, Refresh } from "./styles";

const HomeScreen = () => {
    const [user, setUsers] = useState();
    const [age, setAge] = useState();
    const [loading, setLoading] = useState(false);

    const getUser = useCallback(async () => {
        try {
            setLoading(true);
            const realm = await getRealm();
            const users = await realm.objects('User');

            const age = users.reduce((total, user) => (total += user.age), 0)
            setUsers(users);
            setAge(age / users.length);
            setTimeout(() => {
                setLoading(false);
            }, 500);
        } catch (err) {
            console.log(err)
        }
    }, []);

    return (
        <Container>
            <Title>Relatório</Title>
            <Content>
                <Card>
                    <Subtitle>Total de pessoas cadastradas</Subtitle>
                    <Text>{user && user.length > 0 ? user.length + ' pessoas' : "Nenhum cadastrado"}</Text>
                    <Subtitle>Média de idades</Subtitle>
                    <Text>{age ? age.toFixed(0) + ' anos' : 'Nenhuma informada'}</Text>
                </Card>
                <Refresh activeOpacity={0.6} onPress={getUser}>
                    {loading ?
                        <ActivityIndicator size="small" color="white" />
                        :
                        <Icons name="refresh-cw" color='white' size={32} />
                    }
                </Refresh>
            </Content>
        </Container>
    )
}

export default memo(HomeScreen);