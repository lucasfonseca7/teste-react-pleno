import styled from "styled-components/native";
import { StyleSheet } from "react-native";

export const Container = styled.View`
    flex: 1;
    padding-top: 16px;
    background: #014EF5;
`;

export const Content = styled.View`
    flex: 1;
    background: white;
    border-top-left-radius: 32px;
    border-top-right-radius: 32px;
    padding: 8px;
`;

export const Title = styled.Text`
    font-weight: bold;
    font-size: 22px;
    text-align: center;
    margin-bottom: 16px;
    color: #FFF;
`;

export const Subtitle = styled.Text`
    color: #303030;
    font-weight: bold;
    font-size: 18px;
    margin-top: 8px;
`;

export const Card = styled.View`
    padding: 8px;
    padding-left: 16px;
`;

export const Refresh = styled.TouchableOpacity`
    margin-top: 32px;
    align-self: center;
    width: 100%;
    justify-content: center;
    align-items: center;
    background: #1EB300;
    width: 64px;
    height: 64px;
    border-radius: 32px
    border-width: ${StyleSheet.hairlineWidth}px;
    border-color: #cdcdcd;
    elevation: 4;
`;