import React from "react";
import RootNavigator from "navigation/RootNavigator";

const App = () => {
  return (
    <RootNavigator></RootNavigator>
  )
}

export default App;