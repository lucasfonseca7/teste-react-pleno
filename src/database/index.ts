import Realm from "realm";
import { AdressSchema, UserSchema } from "./schemas";

export default function getRealm() {
    return Realm.open({
        schema: [UserSchema, AdressSchema],
    });
}