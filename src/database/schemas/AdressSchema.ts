export default class AddressSchema {
    static schema = {
        name: 'Address',
        primaryKey: 'id',
        properties: {
            id: { type: 'int', indexed: true },
            cep: 'string',
            bairro: 'string',
            localidade: 'string',
            logradouro: 'string'
        },
    };
}