export default class UserSchema {
    static schema = {
        name: 'User',
        primaryKey: 'id',
        properties: {
            id: { type: 'int', indexed: true },
            name: 'string',
            age: 'int',
            cpf: 'string',
            rg: 'string',
            address: { type: 'Address' }
        },
    };
}