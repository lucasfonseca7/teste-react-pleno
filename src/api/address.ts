import axios from "axios";

interface ILocation {
    cep: string;
}

const apiLocation = axios.create({
    baseURL: 'https://viacep.com.br/ws/',
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 20000,
    validateStatus: () => true
});

const GET_CEP = async (props: ILocation) => {
    const result = await apiLocation.get(`${props.cep}/json`);
    if (result.status === 400) {
        return { erro: true };
    } else {
        return result.data;
    }
};

export { GET_CEP };