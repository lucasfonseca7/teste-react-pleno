import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTab from "./BottomTab";
import { EditScreen } from "features/accredit/screens";

import Icon from "react-native-vector-icons/Feather";
import { TouchableOpacity } from 'react-native';

import WelcomeModal from "./components/welcomeModal";

const Stack = createStackNavigator();

export default function RootNavigator() {

    const [modal, setModal] = React.useState(false);
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    cardStyle: {
                        backgroundColor: '#F5F5F5'
                    }
                }}
            >
                <Stack.Screen
                    name="BottomTab"
                    component={BottomTab}
                    options={{
                        title: 'Gestão de pessoas', headerTitleAlign: "center", headerRight: () => (<TouchableOpacity onPress={() => setModal(true)} style={{ width: 50, justifyContent: "center", alignItems: 'center' }}>
                            <Icon name="help-circle" color="black" size={25} />
                        </TouchableOpacity>)
                    }}
                />
                <Stack.Screen name="EditScreen" component={EditScreen} options={{
                    title: 'Gestão de pessoas', headerTitleAlign: "center"
                }}
                />
            </Stack.Navigator>
            <WelcomeModal isVisible={modal} close={() => setModal(false)}></WelcomeModal>
        </NavigationContainer >
    );
}