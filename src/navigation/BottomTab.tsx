import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen } from "features/feed/screens";
import { RegisterScreen, ListScreen } from "features/accredit/screens";

import Icon from "react-native-vector-icons/Feather";

const Tab = createBottomTabNavigator();

const BottomTab = () => {
    const icons = {
        Home: {
            name: 'home'
        },
        Register: {
            name: 'plus-circle'
        },
        List: {
            name: 'users'
        }
    }

    return (
        <Tab.Navigator
            screenOptions={({ route }) => (
                {
                    tabBarIcon: ({ color, size }) => {
                        const { name } = icons[route.name];
                        return <Icon name={name} size={size} color={color} />
                    }
                }
            )
            }>
            <Tab.Screen name="Home" component={HomeScreen} options={{
                title: "Início",
            }} />
            <Tab.Screen name="List" component={ListScreen} options={{ title: "Usuários" }} />
            <Tab.Screen name="Register" component={RegisterScreen} options={{ title: "Adicionar" }} />
        </Tab.Navigator>
    );
}

export default BottomTab;