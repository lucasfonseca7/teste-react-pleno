import React, { memo } from "react";
import { Modal } from "components";
import { View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";

import { Title, Container, Content, Text } from "./styles";

interface IWelcomeModal {
    close: Function;
    isVisible?: boolean;
}

const WelcomeModal = ({ close, isVisible }: IWelcomeModal) => {
    return (
        <View>
            <Modal isVisible={isVisible}>
                <Container>
                    <Content>
                        <Title>Bem vindo</Title>
                        <View>
                            <Text> • Aplicativo para gerenciamento de pessoas</Text>
                            <Text> • Adicione, edite e remova usuários</Text>
                            <Text> • Acompanhe dados dos usuários</Text>
                        </View>
                    </Content>
                    <TouchableOpacity style={{ height: '20%' }} onPress={() => close()}>
                        <Icon name="x" color="black" size={25} />
                    </TouchableOpacity>
                </Container>
            </Modal>
        </View>
    )
}

export default memo(WelcomeModal);