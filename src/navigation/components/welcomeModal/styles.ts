import styled from "styled-components/native";

export const Title = styled.Text`
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 8px;
`;

export const Container = styled.View`
    flex-direction: row;
    padding-horizontal: 16px;
`;

export const Content = styled.View`
    align-items: center;
    justify-content: center;
    padding-top: 8px;
    padding-bottom: 16px;
`;

export const Text = styled.Text`
    font-size: 16px;
`;