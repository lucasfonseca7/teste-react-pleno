# Teste para Desenvolvedor Pleno

[![e-Precise Logo](https://www.e-precise.com.br/assets/images/logo_com_sombra.png)](https://www.e-precise.com.br/)

### Teste React-native

### Instalação

 - Após a instalação inicial do react native (https://reactnative.dev/docs/environment-setup):
 
- Baixe o repositório
- Rode os seguintes comandos na pasta raiz do projeto:

 1. yarn install
 2. yarn android
 3. yarn start (Caso não encontre o dispositivo na primeira tentativa, utilize o comando yarn reverse && yarn start)